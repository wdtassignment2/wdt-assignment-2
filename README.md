# README #

### What is this repository for? ###

* This repository is for Web Development Technologies, Assignment 2 (Movie Cinplex Website)
* This assignment's aim is to develop an ASP.NET MVC Core Web Application using Visual Studio 2015 and SQL Server 2016.  
* The Web Appplication is an Enterprise-level website for a luxury Cineplex theatres for ABC Cineplex corp.

### How do I get set up? ###

* Clone the repository to your local machine by copy-and-pasting this link: https://bitbucket.org/wdtassignment2/wdt-assignment-2
* Run the database query User_Table_Scripts.sql to generate database tables populated with data
* Open project. Compile and run. The website should open in a browser.

### Basic requirements of the application ###

* The code must be object-oriented, i.e. have more than one class.  
* Do not write bloated class or classes (i.e. a class with a lot of functionality)  
* Do not use many static methods.  
* Use robust exception handling   
* Follow some common C# coding conventions   (https://msdn.microsoft.com/en-us/library/ff926074.aspx) 

### Who do I talk to? ###

* Techathip Jaturawit (s3542360@student.rmit.edu.au)
* Mohamad Mustafa (s3477475@student.rmit.edu.au)