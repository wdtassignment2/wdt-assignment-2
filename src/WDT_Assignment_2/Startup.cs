﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore; // for line 50 IdentityRole
using Microsoft.AspNetCore.Mvc; // For line 57 RequireHttpsAttribute()
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using WDT_Assignment_2.Models;
using WDT_Assignment_2.Services;
using Swashbuckle.Swagger;

namespace WDT_Assignment_2
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets();
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddDbContext<MovieCineplexContext>(options => options.UseSqlServer(Configuration.GetConnectionString("MovieCineplex")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<MovieCineplexContext>()
                .AddDefaultTokenProviders();

            services.AddMvc(options =>
            {
                options.SslPort = 44389;
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(30);
                options.CookieName = ".WDT_Assignment_2.Session";
                options.CookieHttpOnly = true;
            });

            // Add application services for 2factor login authentication.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseIdentity();

            app.UseGoogleAuthentication(new GoogleOptions()
            {
                ClientId = Configuration["Authentication:Google:ClientId"],
                ClientSecret = Configuration["Authentication:Google:ClientSecret"]
            });

            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "all_cinemas",
                  defaults: new { controller = "Cinemas", action = "GetAllCinemas" },
                  template: "Cinemas");

                routes.MapRoute(
                  name: "movies_by_cinema",
                  defaults: new { controller = "Cinemas", action = "GetMoviesByCinema" },
                  template: "Cinemas/{id:int}");

                routes.MapRoute(
                  name: "cinema_first",
                  defaults: new { controller = "Cinemas", action = "CinemaFirst" },
                  template: "Cinema/{cinemaId:int}/{movieId:int}");

                routes.MapRoute(
                  name: "search_cinemas",
                  defaults: new { controller = "Cinemas", action = "SearchCinemas" },
                  template: "Cinema/SearchCinemas/{search}");

                routes.MapRoute(
                  name: "all_movies",
                  defaults: new { controller = "Movies", action = "GetAllMovies" },
                  template: "Movies");

                routes.MapRoute(
                  name: "movie_first",
                  defaults: new { controller = "Movies", action = "MovieFirst" },
                  template: "Movies/{id:int}");

                routes.MapRoute(
                  name: "search_movies",
                  defaults: new { controller = "Movies", action = "SearchMovies" },
                  template: "Movies/SearchMovies/{search}");

                routes.MapRoute(
                  name: "booking",
                  defaults: new { controller = "Booking", action = "Booking" },
                  template: "Booking/{id:int}");


                routes.MapRoute(
                    name: "all_upcoming_movies",
                    defaults: new { controller = "Movies", action = "GetAllUpComingMovies" },
                    template: "Movies/Upcoming");

                routes.MapRoute(
                    name: "single_upcoming_movies",
                    defaults: new { controller = "Movies", action = "GetSingleComingMovie" },
                    template: "Movies/Upcoming/{id:int}");

                routes.MapRoute(
                    name: "get_available_seats",
                    defaults: new { controller = "Booking", action = "GetAvailableSeats" },
                    template: "ShoppingCart/Seats");

                
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Movies}/{action=Index}");

                // Route signin-google to action
                routes.MapRoute(
                    name: "signin-google",
                    defaults: new { controller = "Account", action = "ExternalLoginCallback" },
                    template: "{signin-google}");
            });
        }
    }
}
