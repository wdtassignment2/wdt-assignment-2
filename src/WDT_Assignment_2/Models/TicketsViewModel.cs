﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WDT_Assignment_2.Models
{
    public class TicketsViewModel
    {
        [Range(0,5)]
        [Display(Name = "Adult")]
        public int AdultTickets { get; set; }

        [Range(0, 5)]
        [Display(Name = "Concession")]
        public int ConcessionTickets { get; set; }
    }
}
