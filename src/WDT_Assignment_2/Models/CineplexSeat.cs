﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;


namespace WDT_Assignment_2.Models
{
    public class CineplexSeat
    {
        public int CineplexId { get; set; }
        public int SessionId { get; set; }
        public int SeatId { get; set; }
        public int IsBooked { get; set; }

        public virtual Cineplex Cineplex { get; set; }
        public virtual Session Session { get; set; }
        public virtual Seat Seat { get; set; }
    }
}
