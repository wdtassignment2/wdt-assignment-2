﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WDT_Assignment_2.Models
{
    public partial class Session
    {
        public Session()
        {
            CineplexMovie = new HashSet<CineplexMovie>();
            CineplexSeat = new HashSet<CineplexSeat>();
        }

        public int SessionId { get; set; }
        public string SessionDay { get; set; }
        public string SessionTime { get; set; }
        public int NumberOfBookings { get; set; } // probably going to use this to get number of seats available

        [JsonIgnore] // Prevents self referencing loop
        public virtual ICollection<CineplexMovie> CineplexMovie { get; set; }
        public virtual ICollection<CineplexSeat> CineplexSeat { get; set; }
    }
}
