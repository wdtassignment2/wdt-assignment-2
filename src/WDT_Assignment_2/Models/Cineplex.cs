﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace WDT_Assignment_2.Models
{
    public partial class Cineplex
    {
        public Cineplex()
        {
            CineplexMovie = new HashSet<CineplexMovie>();
            CineplexSeat = new HashSet<CineplexSeat>();
        }

        public int CineplexId { get; set; }
        public string Location { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ImageUrl { get; set; }

        [JsonIgnore]
        public virtual ICollection<CineplexMovie> CineplexMovie { get; set; }
        public virtual ICollection<CineplexSeat> CineplexSeat { get; set; }
    }
}
