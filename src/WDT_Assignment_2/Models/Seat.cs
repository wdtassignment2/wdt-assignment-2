﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace WDT_Assignment_2.Models
{
    public class Seat
    {
        public Seat()
        {
            CineplexSeat = new HashSet<CineplexSeat>();
        }
        public int SeatId { get; set; }
        public string SeatNumber { get; set; }

        public virtual ICollection<CineplexSeat> CineplexSeat { get; set; }
    }
}
