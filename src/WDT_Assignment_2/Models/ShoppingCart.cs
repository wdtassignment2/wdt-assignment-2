﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WDT_Assignment_2.Models
{
    public class ShoppingCart
    {
        // For Json.DeserializeObject<T> to not return the list as null
        [JsonProperty("Tickets")]
        public List<Ticket> Tickets { get; private set; }

        [JsonProperty("AmountDue")]
        public double AmountDue { get; private set; }

        public ShoppingCart()
        {
            Tickets = new List<Ticket>();
        }

        public void UpdateAmountDue()
        {
            // Reset amount for cases where tickets are removed
            AmountDue = 0;
            foreach(Ticket ticket in Tickets)
            {
                AmountDue = AmountDue + ticket.Price;
            }
        }

        //public Ticket FindMatchingTicket(Ticket ticket)
        //{
        //    CineplexMovie details = ticket.SessionDetails;

        //    Ticket result = Tickets.Find(x => x.SessionDetails.CineplexId == details.CineplexId &
        //        x.SessionDetails.MovieId == details.MovieId &
        //        x.SessionDetails.SessionId == details.SessionId &
        //        x.TicketType == ticket.TicketType
        //    );

        //    return result;
        //}

        // No longer needed as Ticket does not have int Quantity
        //public void GroupSimilarTickets()
        //{
        //    //if same cineplex, movie, session, and ticket type, adjust quantity and remove newly added ticket
        //    Ticket lastAdded = Tickets.Last();
        //    if(lastAdded != null)
        //    { 
        //        Ticket sameTicket = FindMatchingTicket(lastAdded);

        //        if (lastAdded != sameTicket)
        //        {
        //            sameTicket.Quantity = sameTicket.Quantity + lastAdded.Quantity;
        //            sameTicket.SetPrice();
        //            Tickets.Remove(lastAdded);
        //        }
        //    }
        //}
    }
}
