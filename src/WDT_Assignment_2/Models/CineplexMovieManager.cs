﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WDT_Assignment_2.Models
{
    public class CineplexMovieManager
    {
        private static CineplexMovieManager _instance;
        private MovieCineplexContext _db = new MovieCineplexContext();

        public static CineplexMovieManager Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new CineplexMovieManager();
                }
                return _instance;
            }
        }

        private CineplexMovieManager() { }

        public Cineplex GetCinemaById(int cinemaId)
        {
            return _db.Cineplex.FirstOrDefault(i => i.CineplexId == cinemaId);
        }

        public List<Movie> GetMoviesByCinemaId(int id)
        {
            return _db.CineplexMovie.Where(i => i.CineplexId == id).Select(m => m.Movie).ToList<Movie>();
        }

        public Movie GetMovieById(int movieId)
        {
            return _db.Movie.FirstOrDefault(p => p.MovieId == movieId);
        }
        public Session GetSessionById(int sessionId)
        {
            return _db.Session.FirstOrDefault(i => i.SessionId == sessionId);
        }

        public List<Session> GetSessionsByCinemaIdAndMovieId(int cinemaId, int movieId)
        {
            List<Session> session = _db.CineplexMovie.Where(c => c.CineplexId == cinemaId & c.MovieId == movieId).Select(s => s.Session).ToList();

            return session;
        }

        public CineplexMovie GetCineplexMovieByAllId(int cinemaId, int movieId, int sessionId)
        {
            CineplexMovie cineplexMovie = _db.CineplexMovie.FirstOrDefault(i => i.CineplexId == cinemaId & i.MovieId == movieId & i.SessionId == sessionId);

            return cineplexMovie;
        }
    }
}
