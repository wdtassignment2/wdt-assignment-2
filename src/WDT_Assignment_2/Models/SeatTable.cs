﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WDT_Assignment_2.Models
{
    public class SeatTable
    {
        public List<SelectListItem> SeatList { get; set; }
        public int SeatId { get; set; }
    }

}
