﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WDT_Assignment_2.Models
{
    public class CreditCard
    {
        [Required]
        [CreditCard]
        [Display(Name = "card number")]
        public int CardNumber { get; set; }

        [Required]
        [Range(1, 12, ErrorMessage = "Please enter a number from 1 to 12")]
        [Display(Name = "month")]
        public int ExpireMonth { get; set; }

        [Required]
        [Range(0,9999, ErrorMessage = "Please enter a valid year")]
        [Display(Name = "year")]
        public int ExpireYear { get; set; }

        [Required]
        [Range(0,999, ErrorMessage = "Please enter a number")]
        [Display(Name = "CVV")]
        public int CVV { get; set; }

        public CreditCard() { }

    }
}
