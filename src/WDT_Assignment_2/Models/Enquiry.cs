﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WDT_Assignment_2.Models
{
    public partial class Enquiry
    {
        public int EnquiryId { get; set; }

        // For form validation
        [Required]
        [EmailAddress]
        [StringLength(30, MinimumLength = 10)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(250, MinimumLength = 20, ErrorMessage = "Please enter a longer message.")]
        [Display(Name = "Enquiry")]
        public string Message { get; set; }
    }
}
