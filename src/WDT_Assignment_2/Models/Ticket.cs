﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WDT_Assignment_2.Models
{
    public class Ticket
    {
        public CineplexMovie SessionDetails { get; private set; }
        public string TicketType { get; private set; }
        public string SeatNumber { get; private set; }
        public double Price { get; private set; }

        public Ticket(CineplexMovie sessionDetails, string ticketType, string seatNumber)
        {
            SessionDetails = sessionDetails;
            TicketType = ticketType;
            SeatNumber = seatNumber;
            SetPrice();
        }

        public void SetTicketType(string ticketType)
        {
            TicketType = ticketType;
            SetPrice();
        }

        public void SetSeatNumber(string seatNumber)
        {
            SeatNumber = seatNumber;
        }

        public void SetPrice()
        {
            if (TicketType.ToLower() == "adult")
            {
                Price = 45;
            }
            else if (TicketType.ToLower() == "concession")
            {
                Price = 20;
            }
        }
    }
}
