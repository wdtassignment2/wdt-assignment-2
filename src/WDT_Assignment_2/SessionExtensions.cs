﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

// References:
// Blackboard MVC_VS2015.pdf page 229
// List type compatibility tutorial
// https://exprotocol.com/2016/10/27/asp-net-core-1-0-objects-in-sessions/

namespace WDT_Assignment_2
{
    public static class SessionExtensions
    {
        public static void Set(this ISession session, string key, object value)
        {
            string json = JsonConvert.SerializeObject(value);
            byte[] serializedResult = System.Text.Encoding.UTF8.GetBytes(json);
            session.Set(key, serializedResult);
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.Get(key);
            string json = System.Text.Encoding.UTF8.GetString(value);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }

        public static bool IsExists(this ISession session, string key)
        {
            return session.Get(key) != null;
        }
    }
}
