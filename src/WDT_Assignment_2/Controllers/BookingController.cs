﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WDT_Assignment_2.Controllers
{
    public class BookingController : Controller
    {
        private MovieCineplexContext _db;
        private CineplexMovieManager cineplexMM = CineplexMovieManager.Instance;

        public BookingController(MovieCineplexContext db)
        {
            _db = db;
        }

        //public IActionResult Index()
        //{
        //    return View();
        //}

        public IActionResult Booking(int sessionId)
        {
            // Get Cineplex, Movie, and Session to search for CineplexMovie and store in http session
            Cineplex cineplex = HttpContext.Session.Get<Cineplex>("CurrentCineplex");
            Movie movie = HttpContext.Session.Get<Movie>("CurrentMovie");

            Session session = (Session)cineplexMM.GetSessionById(sessionId);
            CineplexMovie cineplexMovie = (CineplexMovie)cineplexMM.GetCineplexMovieByAllId(cineplex.CineplexId, movie.MovieId, sessionId);

            HttpContext.Session.Set("CurrentCineplexMovie", cineplexMovie);

            // Checks quantity of tickets in cart and adjusts how much more the user can add to cart
            if (HttpContext.Session.IsExists("ShoppingCart"))
            {
                ShoppingCart shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                ViewBag.TicketsLeft = 5 - shoppingCart.Tickets.Count;
            }
            else
            {
                // Defaults to 5 tickets limit if no shopping cart
                ViewBag.TicketsLeft = 5;
            }

            ViewBag.Movie = movie;
            ViewBag.Session = session;
            ViewBag.Cineplex = cineplex;

            return View();
        }

        [HttpPost]
        public IActionResult BookingPost(TicketsViewModel model)
        {
            HttpContext.Session.Set("CurrentTicketsQty", model);

            // Go to AddToCart() after selecting tickets
            return RedirectToAction("AddToCart", "ShoppingCart");
        }

        public IActionResult GetAvailableSeats()
        {
            CineplexMovie cineplexMovie = HttpContext.Session.Get<CineplexMovie>("CurrentCineplexMovie");
            Cineplex cineplex = cineplexMovie.Cineplex;
            Movie movie = cineplexMovie.Movie;
            Session session = cineplexMovie.Session;

            TicketsViewModel ticketsViewModel = HttpContext.Session.Get<TicketsViewModel>("CurrentTicketsQty");

            // Get seats that aren't booked
            var seatTable = new SeatTable();
            seatTable.SeatList = _db.CineplexSeat.Where(c => c.CineplexId == cineplex.CineplexId & c.SessionId == session.SessionId & c.IsBooked == 0).Select(c => new SelectListItem
            {
                Value = c.Seat.SeatId.ToString(),
                Text = c.Seat.SeatNumber
            }).ToList();
            ;

            List<Seat> seats = (List<Seat>)_db.CineplexSeat.Where(c => c.CineplexId == cineplex.CineplexId & c.SessionId == session.SessionId & c.IsBooked == 0).Select(c => c.Seat).ToList();

            ViewBag.Movie = movie;
            ViewBag.Session = session;
            ViewBag.Cineplex = cineplex;
            ViewBag.Seat = seatTable;
            ViewBag.Seats = seats;
            ViewBag.TicketsViewModel = ticketsViewModel;

            ViewData["Seats"] = seatTable.SeatList;

            return View("Seats");
        }
    }
}
