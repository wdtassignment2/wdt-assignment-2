﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WDT_Assignment_2.Controllers
{
    /*
     * Code used from this tutorial
     * https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api
    */

    [Route("api/movieApi")]
    public class MovieAPIController : Controller
    {
        private readonly MovieCineplexContext _context;

        public MovieAPIController(MovieCineplexContext context)
        {
            _context = context;
            if (_context.Movie.Count() == 0)
            {
                _context.Movie.Add(new Movie { MovieId = 1, Title = "Movie 1", ShortDescription = "Short Discription", LongDescription = "Long Discription", ImageUrl = "~images/placeholder-thumbnail1.png", Price = 0 });
                _context.SaveChanges();
            }
        }
        // GET: api/CineplexAPI/allmovies
        [HttpGet]
        public IEnumerable<Movie> GetAllMovies()
        {
            return _context.Movie.ToList();
        }

        // GET api/CineplexAPI/allmovies/5
        [HttpGet("{id}", Name = "GetMovies")]
        public IActionResult GetMovieById(int id)
        {
            var movie = _context.Movie.FirstOrDefault(m => m.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }
            return new ObjectResult(movie);
        }

        // POST api/CineplexAPI
        [HttpPost]
        public IActionResult Post([FromBody]Movie movie)
        {
            if (movie == null)
            {
                return BadRequest();
            }

            _context.Movie.Add(movie);
            _context.SaveChanges();

            return CreatedAtRoute("GetMovies", new { id = movie.MovieId }, movie);
        }

        // PUT api/CineplexAPI/5
        [HttpPut("{id}")]
        public IActionResult PutMovie(int id, [FromBody]Movie m)
        {
            if (m == null || m.MovieId != id)
            {
                return BadRequest();
            }

            var movie = _context.Movie.FirstOrDefault(t => t.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }


            movie.Price = m.Price;
            movie.ImageUrl = m.ImageUrl;
            movie.LongDescription = m.LongDescription;
            movie.ShortDescription = m.ShortDescription;
            movie.Title = m.Title;
            movie.MovieId = m.MovieId;

            _context.Movie.Update(movie);
            _context.SaveChanges();
            return new NoContentResult();
        }

        // DELETE api/CineplexAPI/5
        [HttpDelete("{id}")]
        public IActionResult DeleteMovie(int id)
        {
            var movie = _context.Movie.First(t => t.MovieId == id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }

    [Route("api/cinemaApi")]
    public class CineplexAPIController : Controller
    {
        private readonly MovieCineplexContext _context;

        public CineplexAPIController(MovieCineplexContext context)
        {
            _context = context;
            if (_context.Cineplex.Count() == 0)
            {
                _context.Cineplex.Add(new Cineplex { CineplexId = 1, Location = "Location 1", ShortDescription = "Short Discription", LongDescription = "Long Discription", ImageUrl = "~images/placeholder-thumbnail1.png" });
                _context.SaveChanges();
            }
        }

        // GET: api/cinemaApi
        [HttpGet]
        public IEnumerable<Cineplex> GetAllCinemas()
        {
            return _context.Cineplex.ToList();
        }


        // GET api/cinemaApi/5
        [HttpGet("{id}", Name = "GetCineplex")]
        public IActionResult GetCinemaById(int id)
        {
            var cinema = _context.Cineplex.FirstOrDefault(m => m.CineplexId == id);
            if (cinema == null)
            {
                return NotFound();
            }
            return new ObjectResult(cinema);
        }

        // POST api/CineplexAPI
        [HttpPost]
        public IActionResult Post([FromBody]Cineplex cinema)
        {
            if (cinema == null)
            {
                return BadRequest();
            }

            _context.Cineplex.Add(cinema);
            _context.SaveChanges();

            return CreatedAtRoute("GetMovies", new { id = cinema.CineplexId }, cinema);
        }

        // PUT api/CineplexAPI/5
        [HttpPut("{id}")]
        public IActionResult PutCineplex(int id, [FromBody]Cineplex m)
        {
            if (m == null || m.CineplexId != id)
            {
                return BadRequest();
            }

            var cineplex = _context.Cineplex.FirstOrDefault(t => t.CineplexId == id);
            if (cineplex == null)
            {
                return NotFound();
            }


            cineplex.ImageUrl = m.ImageUrl;
            cineplex.Location = m.Location;
            cineplex.LongDescription = m.LongDescription;
            cineplex.ShortDescription = m.ShortDescription;

            _context.Cineplex.Update(cineplex);
            _context.SaveChanges();
            return new NoContentResult();
        }

        // DELETE api/CineplexAPI/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCineplex(int id)
        {
            var cineplex = _context.Cineplex.First(t => t.CineplexId == id);
            if (cineplex == null)
            {
                return NotFound();
            }

            _context.Cineplex.Remove(cineplex);
            _context.SaveChanges();
            return new NoContentResult();
        }
    }
}
