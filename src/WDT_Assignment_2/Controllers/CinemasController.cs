﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;
using MoreLinq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WDT_Assignment_2.Controllers
{
    public class CinemasController : Controller
    {
        private MovieCineplexContext _db;
        private CineplexMovieManager cineplexMM = CineplexMovieManager.Instance;

        public CinemasController(MovieCineplexContext db)
        {
            _db = db;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetAllCinemas()
        {
            ViewData["AllCinemas"] = _db.Cineplex.ToList<Cineplex>();
            
            return View("AllCinemas");
        }

        public IActionResult GetMoviesByCinema(int id)
        {
            List<Movie> movies = new List<Movie>(cineplexMM.GetMoviesByCinemaId(id));
            var filteredCinepelx = movies.DistinctBy(x => x.MovieId).ToList();
            ViewData["MoviesByCinema"] = filteredCinepelx;

            return View("MoviesByCinema");
        }

        public IActionResult SearchCinemas(string search)
        {
            List<Cineplex> searchedCinemas = new List<Cineplex>();

            if (!String.IsNullOrEmpty(search))
            {
                searchedCinemas = _db.Cineplex.Where(m => m.Location.Contains(search)).ToList<Cineplex>();

                ViewData["AllCinemas"] = searchedCinemas;
            }
            else
            {
                ViewData["AllCinemas"] = _db.Movie.ToList();
            }

            return View("AllCinemas");
        }

        public IActionResult CinemaFirst(int cinemaId, int movieId)
        {
            Cineplex cineplex = (Cineplex)cineplexMM.GetCinemaById(cinemaId);
            Movie movie = (Movie)cineplexMM.GetMovieById(movieId);
            List<Session> sessions = cineplexMM.GetSessionsByCinemaIdAndMovieId(cinemaId, movieId);

            // Does not deserialize properly
            //{
            //    {cineplex, movie}
            //};
            //HttpContext.Session.Set("CurrentCinemaMovie", currentCinemaMovie);
            //HttpContext.Session.Set("CurrentCinemaMovie", currentCinemaMovie);
            HttpContext.Session.Set("CurrentCineplex", cineplex);
            HttpContext.Session.Set("CurrentMovie", movie);

            Dictionary<Movie, List<Session>> dict = new Dictionary<Movie, List<Session>>
            {
                {movie, sessions}
            };

            ViewData["CinemaFirst"] = dict;

            return View("CinemaFirst");
        }
    }
}
