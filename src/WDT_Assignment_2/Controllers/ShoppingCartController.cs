﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WDT_Assignment_2.Controllers
{
    public class ShoppingCartController : Controller
    {
        private MovieCineplexContext _db;
        private CineplexMovieManager cineplexMM = CineplexMovieManager.Instance;

        public ShoppingCartController(MovieCineplexContext db)
        {
            _db = db;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            if (HttpContext.Session.IsExists("ShoppingCart"))
            {
                ShoppingCart shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                ViewBag.ShoppingCart = shoppingCart;
            }

            return View();
        }

        public IActionResult AddToCart()
        {
            TicketsViewModel ticketsViewModel = HttpContext.Session.Get<TicketsViewModel>("CurrentTicketsQty");
            int totalQty = ticketsViewModel.AdultTickets + ticketsViewModel.ConcessionTickets;

            if (totalQty > 0)
            {
                CineplexMovie cineplexMovie = HttpContext.Session.Get<CineplexMovie>("CurrentCineplexMovie");

                ShoppingCart shoppingCart;
                if (HttpContext.Session.IsExists("ShoppingCart"))
                {
                    shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                }
                else
                {
                    shoppingCart = new ShoppingCart();
                }

                if (ticketsViewModel.AdultTickets > 0)
                {
                    //Ticket ticket = new Ticket(cineplexMovie, "adult", model.AdultTickets);
                    //shoppingCart.Tickets.Add(ticket);
                    for(int i = 0; i < ticketsViewModel.AdultTickets; i++)
                    {
                        Ticket ticket = new Ticket(cineplexMovie, "adult", "0");
                        shoppingCart.Tickets.Add(ticket);
                    }
                }

                if (ticketsViewModel.ConcessionTickets > 0)
                {
                    //Ticket ticket = new Ticket(cineplexMovie, "concession", model.ConcessionTickets);
                    //shoppingCart.Tickets.Add(ticket);
                    for (int i = 0; i < ticketsViewModel.ConcessionTickets; i++)
                    {
                        Ticket ticket = new Ticket(cineplexMovie, "concession", "0");
                        shoppingCart.Tickets.Add(ticket);
                    }
                }

                //shoppingCart.GroupSimilarTickets();
                shoppingCart.UpdateAmountDue();

                HttpContext.Session.Set("ShoppingCart", shoppingCart);
            }

            // Go to Cart page after adding items to cart
            return RedirectToAction("Index");
        }

        public IActionResult RemoveItem(int itemId)
        {
            if (HttpContext.Session.IsExists("ShoppingCart"))
            {
                ShoppingCart shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                shoppingCart.Tickets.RemoveAt(itemId);
                shoppingCart.UpdateAmountDue();

                HttpContext.Session.Set("ShoppingCart", shoppingCart);
            }

            // Go back to cart once cart item is removed
            return RedirectToAction("Index");
        }

        public IActionResult RemoveAll()
        {
            ShoppingCart shoppingCart = new ShoppingCart();

            HttpContext.Session.Set("ShoppingCart", shoppingCart);

            return RedirectToAction("Index");
        }

        public IActionResult Checkout()
        {
            if (HttpContext.Session.IsExists("ShoppingCart"))
            {
                ShoppingCart shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                ViewBag.ShoppingCart = shoppingCart;
            }

            return View();
        }

        [HttpPost]
        public IActionResult PaymentSummary()
        {
            if (HttpContext.Session.IsExists("ShoppingCart"))
            {
                ShoppingCart shoppingCart = HttpContext.Session.Get<ShoppingCart>("ShoppingCart");

                // For displaying the payment summary
                ViewBag.ShoppingCart = shoppingCart;

                // Empty the cart after payment is processed
                HttpContext.Session.Set("ShoppingCart", new ShoppingCart());
            }
            else
            {
                // Return to cart page if session has expired when paying
                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
