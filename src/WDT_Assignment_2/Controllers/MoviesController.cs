﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;
using System.Data.SqlClient;
using System.Data;
using MoreLinq;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WDT_Assignment_2.Controllers
{
    public class MoviesController : Controller
    {
        private MovieCineplexContext _db;

        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public MoviesController(
            MovieCineplexContext db,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _db = db;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {
            // Cheeky login session checker
            if(!HttpContext.Session.IsExists("LoggedIn") && _signInManager.IsSignedIn(User))
            {
                return RedirectToAction("SessionLogOff", "Account");
            }

            ViewData["AllMovies"] = _db.Movie.ToList<Movie>();

            return View();
        }

        public IActionResult UpcomingMovies()
        {
            ViewData["AllUpcoming"] = _db.MovieComingSoon.ToList();

            return View();
        }

        //invoke using localhost/movies/id
        public IActionResult MovieFirst(int id)
        {

            CineplexMovie cineplexMovie = _db.CineplexMovie.FirstOrDefault(p => p.MovieId == id);
            List<Cineplex> cineplex = _db.CineplexMovie.Where(p => p.MovieId == id).Select(c => c.Cineplex).ToList();
            var filteredCinepelx = cineplex.DistinctBy(x => x.CineplexId).ToList();
            Movie movie = _db.CineplexMovie.Where(p => p.MovieId == id).Select(m => m.Movie).FirstOrDefault();

            Dictionary<CineplexMovie, List<Cineplex>> dict = new Dictionary<CineplexMovie, List<Cineplex>>
            {
                {cineplexMovie, filteredCinepelx }
            };

            ViewData["MovieFirst"] = dict;
            return View("MovieFirst");
        }

        //invoke using link localhost/movies/all
        public IActionResult GetAllMovies()
        {
            List<Movie> movieList = _db.Movie.ToList<Movie>();
            var filteredMovieList = movieList.DistinctBy(x => x.MovieId).ToList();
            ViewData["Movies"] = filteredMovieList;
            return View("Movies");
        }

        public IActionResult SearchMovies(string search)
        {
            List<Movie> searchedMovies = new List<Movie>();

            if (!String.IsNullOrEmpty(search))
            {
                searchedMovies = _db.Movie.Where(m => m.Title.Contains(search)).ToList<Movie>();

                ViewData["Movies"] = searchedMovies;
            }
            else
            {
                ViewData["Movies"] = _db.Movie.ToList();
            }

            return View("Movies");
        }
    }
}
