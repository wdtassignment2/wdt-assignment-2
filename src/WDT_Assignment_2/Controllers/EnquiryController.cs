﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WDT_Assignment_2.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WDT_Assignment_2.Controllers
{
    public class EnquiryController : Controller
    {
        private MovieCineplexContext _db;

        public EnquiryController(MovieCineplexContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(Enquiry model)
        {
            if(!ModelState.IsValid)
            {
                // Prevent null inputs on submit
                return View(model);
            }

            var enquiry = new Enquiry();
            enquiry.Email = model.Email;
            enquiry.Message = model.Message;

            _db.Enquiry.Add(enquiry);
            _db.SaveChanges();

            return RedirectToAction("Index");//View(model);
        }
    }
}
